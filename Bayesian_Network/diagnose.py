import pdb
from heapq import *
import sys
from collections import defaultdict

def evaluateQuesOne(diseaseList,patient):
    pDiseaseDict = {}
    for i in range(0,len(diseaseList)):
        posDis=[]
        negDis=[]
        patDisPres=[]
        patDisPres = eval('patient[i]')
        posDis.append(diseaseList[i]['dProb'])
        negDis.append(1-diseaseList[i]['dProb'])
        for j in range(0,len(patDisPres)):
            if(patDisPres[j]=='T'):
                posDis.append(diseaseList[i]["finTrue"][j])
                negDis.append(diseaseList[i]["finFalse"][j])
            if(patDisPres[j]=='F'):
                posDis.append(1-diseaseList[i]["finTrue"][j])
                negDis.append(1-diseaseList[i]["finFalse"][j])
        pDisT=1
        pDisF=1
        pDisease = 0
        for j in range(0,len(posDis)):
            pDisT = pDisT * posDis[j]
            pDisF = pDisF * negDis[j]
        pDisease = pDisT/(pDisT + pDisF)
        pDiseaseDict[diseaseList[i]['dName']] = format(round(pDisease,4),'.4f')
    return pDiseaseDict

"""
Function for finding the maximum probability and minimum probability given the lab testes for unknown findings

"""
    

def evaluateQuesTwo(diseaseList,patient):
    pDiseaseDict = {}
    for i in range(0,len(diseaseList)):
        posDis=[]
        negDis=[]
        patDisPres=[]
        patDisPres = eval('patient[i]')
        posDis.append(diseaseList[i]['dProb'])
        negDis.append(1-diseaseList[i]['dProb'])
        for j in range(0,len(patDisPres)):
            if(patDisPres[j]=='T'):
                posDis.append(diseaseList[i]["finTrue"][j])
                negDis.append(diseaseList[i]["finFalse"][j])
            if(patDisPres[j]=='F'):
                posDis.append(1-diseaseList[i]["finTrue"][j])
                negDis.append(1-diseaseList[i]["finFalse"][j])
        pDisT=1
        pDisF=1
        pDisease = 0
        for j in range(0,len(posDis)):
            pDisT = pDisT * posDis[j]
            pDisF = pDisF * negDis[j]
        pDisease = pDisT/(pDisT + pDisF)
        maxProb = pDisease
        minProb = pDisease
        maxNumer = pDisT
        maxDenom = pDisF
        minNumer = pDisT
        minDenom = pDisF
        for j in range(0,len(patDisPres)):
            if(patDisPres[j]=='U'):
                
                """ checking with u=true """
                
                newNumerMaxTrue = maxNumer * diseaseList[i]["finTrue"][j]
                newDenomMaxTrue = maxDenom * diseaseList[i]["finFalse"][j]
                newProbTrueMax = newNumerMaxTrue/(newNumerMaxTrue + newDenomMaxTrue)

                newNumerMinTrue = minNumer * (diseaseList[i]["finTrue"][j])
                newDenomMinTrue = minDenom * (diseaseList[i]["finFalse"][j])
                newProbTrueMin = newNumerMinTrue/(newNumerMinTrue + newDenomMinTrue)

                if(newProbTrueMax > maxProb):
                    maxProb = newProbTrueMax
                    uTrueMax = True

                if(newProbTrueMin < minProb):
                    minProb = newProbTrueMin
                    uTrueMin = True

                    
                """ checking with u=false """

                newNumerMaxFalse = maxNumer * (1-diseaseList[i]["finTrue"][j])
                newDenomMaxFalse = maxDenom * (1-diseaseList[i]["finFalse"][j])
                newProbFlaseMax = newNumerMaxFalse/(newNumerMaxFalse + newDenomMaxFalse)

                newNumerMinFalse = minNumer * (1-diseaseList[i]["finTrue"][j])
                newDenomMinFalse = minDenom * (1-diseaseList[i]["finFalse"][j])
                newProbFalseMin = newNumerMinFalse/(newNumerMinFalse + newDenomMinFalse)

                if(newProbFlaseMax > maxProb):
                    maxProb = newProbFlaseMax
                    uTrueMax = False

                if(newProbFalseMin < minProb):
                    minProb = newProbFalseMin
                    uTrueMin = False

                if(uTrueMax):
                    maxNumer = maxNumer * diseaseList[i]["finTrue"][j]
                    maxDenom = maxDenom * diseaseList[i]["finFalse"][j]
                else:
                    maxNumer = maxNumer * (1-diseaseList[i]["finTrue"][j])
                    maxDenom = maxDenom * (1-diseaseList[i]["finFalse"][j])

                if(uTrueMin):
                    minNumer = minNumer * diseaseList[i]["finTrue"][j]
                    minDenom = minDenom * diseaseList[i]["finFalse"][j]
                else:
                    minNumer = minNumer * (1-diseaseList[i]["finTrue"][j])
                    minDenom = minDenom * (1-diseaseList[i]["finFalse"][j])
                    

        pDiseaseDict[diseaseList[i]['dName']] = [format(round(minProb,4),'.4f'),format(round(maxProb,4),'.4f')]    
    return pDiseaseDict
        

def evaluateQuesThree(diseaseList,patient):
    pDiseaseDict = {}
    for i in range(0,len(diseaseList)):
        posDis=[]
        negDis=[]
        patDisPres=[]
        patDisPres = eval('patient[i]')
        posDis.append(diseaseList[i]['dProb'])
        negDis.append(1-diseaseList[i]['dProb'])
        for j in range(0,len(patDisPres)):
            if(patDisPres[j]=='T'):
                posDis.append(diseaseList[i]["finTrue"][j])
                negDis.append(diseaseList[i]["finFalse"][j])
            if(patDisPres[j]=='F'):
                posDis.append(1-diseaseList[i]["finTrue"][j])
                negDis.append(1-diseaseList[i]["finFalse"][j])
        pDisT=1
        pDisF=1
        pDisease = 0
        for j in range(0,len(posDis)):
            pDisT = pDisT * posDis[j]
            pDisF = pDisF * negDis[j]
        pDisease = pDisT/(pDisT + pDisF)
        pDisNumer = pDisT
        pDisDenom = pDisF
        maxIncr = 0
        maxDecr = 0
        findMaxDis = "None"
        findMinDis = "None"
        findMaxFlag = 'N'
        findMinFlag = 'N'
        for j in range(0,len(patDisPres)):
            if(patDisPres[j]=='U'):
                
                """checking with u = true """
                
                newMaxIncr = 0
                newMaxDecr = 0
                newPDisNum = pDisNumer * diseaseList[i]["finTrue"][j]
                newPDisDen = pDisDenom * diseaseList[i]["finFalse"][j]
                newPDis = newPDisNum / (newPDisNum + newPDisDen)
                if(newPDis > pDisease):
                    newMaxIncr = newPDis - pDisease
                elif(newPDis < pDisease):
                    newMaxDecr = pDisease - newPDis
                else:
                    newMaxIncr = 0
                    newMaxDecr = 0

                if(newMaxIncr > maxIncr):
                    findMaxDis = diseaseList[i]['finding'][j]
                    findMaxFlag = 'T'
                    maxIncr = newMaxIncr

                if(newMaxDecr > maxDecr):
                    findMinDis = diseaseList[i]['finding'][j]
                    findMinFlag = 'T'
                    maxDecr = newMaxDecr

                """checking with u = false """

                newMaxIncr = 0
                newMaxDecr = 0
                newPDisNum = pDisNumer * (1-diseaseList[i]["finTrue"][j])
                newPDisDen = pDisDenom * (1-diseaseList[i]["finFalse"][j])
                newPDis = newPDisNum / (newPDisNum + newPDisDen)
                if(newPDis > pDisease):
                    newMaxIncr = newPDis - pDisease
                elif(newPDis < pDisease):
                    newMaxDecr = pDisease - newPDis
                else:
                    newMaxIncr = 0
                    newMaxDecr = 0

                if(newMaxIncr > maxIncr):
                    findMaxDis = diseaseList[i]['finding'][j]
                    findMaxFlag = 'F'
                    maxIncr = newMaxIncr

                if(newMaxDecr > maxDecr):
                    findMinDis = diseaseList[i]['finding'][j]
                    findMinFlag = 'F'
                    maxDecr = newMaxDecr

        pDiseaseDict[diseaseList[i]['dName']] = [findMaxDis,findMaxFlag,findMinDis,findMinFlag]  
    return pDiseaseDict


inputFile = open(sys.argv[2], 'r')
outputFile = open(sys.argv[2]+'_inference.txt', 'w')
firstLine = inputFile.readline()
firstLineList = firstLine.split()
diseaseCount = int(firstLineList[0])
patientCount = int(firstLineList[1])
diseaseList=[]
patientList=[]
diseaseDict = {}
patientDict = {}
for i in range(0,diseaseCount):
    diseaseDict.clear()
    line = inputFile.readline()
    lineList = line.split()
    diseaseDict['dName'] = lineList[0]
    diseaseDict['dProb'] = float(lineList[2])
    diseaseDict['noOfFinding'] = int(lineList[1])
    line = inputFile.readline()
    lineList = []
    lineList = eval(line)
    diseaseDict['finding'] = lineList
    line = inputFile.readline()
    lineList = []
    lineList = eval(line)
    diseaseDict['finTrue'] = lineList
    line = inputFile.readline()
    lineList = []
    lineList = eval(line)
    diseaseDict['finFalse'] = lineList
    diseaseList.append(dict(diseaseDict))
    
for i in range(0,patientCount):
    patientDict.clear()
    patientDict['pName'] = "pat" + str(i)
    for j in range(0,diseaseCount):
        line = inputFile.readline()
        lineList = []
        lineList = eval(line)
        patientDict[j] = lineList
    patientList.append(dict(patientDict))



for i in range(0,patientCount):
    patDisDict = {}
    outputFile.write("Patient-" + str(i+1)+"\n")
    patDisDict = evaluateQuesOne(diseaseList,dict(patientList[i]))
    outputFile.write(str(patDisDict) + "\n")
    patDisDict = evaluateQuesTwo(diseaseList,dict(patientList[i]))
    outputFile.write(str(patDisDict) + "\n")
    #pdb.set_trace()
    patDisDict = evaluateQuesThree(diseaseList,dict(patientList[i]))
    outputFile.write(str(patDisDict) + "\n")
    
"""    
evaluateQuesOne(diseaseList,patientList)
evaluateQuesTwo(diseaseList,patientList)
evaluateQuesThree(diseaseList,patientList)
"""

inputFile.close()
outputFile.close()
