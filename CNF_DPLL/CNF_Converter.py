import pdb
from heapq import *
import sys
from collections import defaultdict

""" Recursive Function for removing bi conditional from the input
    Output returned will be free from biconditional operator"""
def BiConditionalFree(stringExpr):
    print stringExpr
    if(len(stringExpr) > 1): 
        expr = eval('stringExpr')
        optr = expr[0]

    else:
        optr=stringExpr 
    
    if(len(optr) == 1):
        return optr
        """if the input expression is a literal return it"""
    elif(optr == 'not'):
        operand1=BiConditionalFree(expr[1])
        retExpr = ['not']
        retExpr.append(operand1)
        return retExpr
        """expressions other than iff are recursively traversed to find nested 'iff' operator"""

    elif(optr == 'and'):
        retExpr = ['and']
        for items in range(1,len(expr)):
            operand1 = BiConditionalFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than iff are recursively traversed to find nested 'iff' operator
            for loop traverses all the operands of 'and' and 'or' operator"""
        
    elif(optr == 'or'):
        retExpr = ['or']
        for items in range(1,len(expr)):
            operand1 = BiConditionalFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than iff are recursively traversed to find nested 'iff' operator
            for loop traverses all the operands of 'and' and 'or' operator"""
    
    elif(optr == 'implies'):
        retExpr = ['implies']
        operand1=BiConditionalFree(expr[1])
        operand2=BiConditionalFree(expr[2])
        retExpr.append(operand1)
        retExpr.append(operand2)
        return retExpr
        """expressions other than iff are recursively traversed to find nested 'iff' operator"""

    elif(optr == 'iff'):
        retExpr = ['and']
        tempExpr=['implies']
        operand1=BiConditionalFree(expr[1])
        operand2=BiConditionalFree(expr[2])
        tempExpr.append(operand1)
        tempExpr.append(operand2)
        retExpr.append(tempExpr)
        tempExpr=['implies']
        tempExpr.append(operand2)
        tempExpr.append(operand1)
        retExpr.append(tempExpr)
        return retExpr
        """ the above code applies the transformation (iff a, b) to (a -> b and b -> a) """
    else:
        print "Improper logical statement"
        return
        
""" Function for removing Implication """

def ImpliesFree(stringExpr) :
    print stringExpr
    if(len(stringExpr) > 1): 
        expr = eval('stringExpr')
        optr = expr[0]

    else:
        optr=stringExpr
        
    
    if(len(optr) == 1):
        return optr
        """if the input expression is a literal return it"""
    elif(optr == 'not'):
        operand1=ImpliesFree(expr[1])
        retExpr = ['not']
        retExpr.append(operand1)
        return retExpr
        """expressions other than implies are recursively traversed to find nested 'implies' operator"""

    elif(optr == 'and'):
        retExpr = ['and']
        for items in range(1,len(expr)):
            operand1 = ImpliesFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than implies are recursively traversed to find nested 'implies' operator"""
        
    elif(optr == 'or'):
        retExpr = ['or']
        for items in range(1,len(expr)):
            operand1 = ImpliesFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than implies are recursively traversed to find nested 'implies' operator"""

    elif(optr == 'implies'):
        tempOpA = ['not']
        tempOpA.insert(1,expr[1])
        operand1 = ImpliesFree(tempOpA)
        operand2 = ImpliesFree(expr[2])
        retExpr = ['or']
        retExpr.append(operand1)
        retExpr.append(operand2)
        return retExpr
        """applies the transformation rule (a -> b) : ~a V b and recursively call the function to find nested 'implies' operators """

    else:
        print "Improper logical statement"
        return

""" Function for removing negation """

def NegationFree(stringExpr):
    print stringExpr
    if(len(stringExpr) > 1):
        expr = eval('stringExpr')
        optr = expr[0]
    else:
        optr=stringExpr

    if(len(optr) == 1):
        return optr
        """if the input expression is a literal return it"""       
    elif(optr == 'and'):
        retExpr = ['and']
        for items in range(1,len(expr)):
            operand1 = NegationFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than 'not' are recursively traversed to find nested 'not' operator"""
    elif(optr == 'or'):
        retExpr = ['or']
        for items in range(1,len(expr)):
            operand1 = NegationFree(expr[items])
            retExpr.append(operand1)
        return retExpr
        """expressions other than 'not' are recursively traversed to find nested 'not' operator"""
    elif(optr == 'not'):
        if(len(expr[1]) > 1):
            nextExpr = eval('expr[1]')
            nextOptr = nextExpr[0]
            if (nextOptr == 'not'):
                operand1 = NegationFree(nextExpr[1])
                return operand1
                """Transformation rule applying double negation [~(~a) = a] """
            elif (nextOptr == 'and'):
                retExpr = ['or']
                for newItems in range(1,len(nextExpr)):
                    newOperand = ['not']
                    newOperA=NegationFree(nextExpr[newItems])
                    newOperand.append(newOperA)
                    newOperand=NegationFree(newOperand)
                    retExpr.append(newOperand)
                return retExpr
                """ Transformation applying demorgans law ~(a and b) : ~a V ~b and calls function recursively to find nested 'not' opertors"""
            elif (nextOptr == 'or'):
                retExpr = ['and']
                for newItems in range(1,len(nextExpr)):
                    newOperand = ['not']
                    newOperA=NegationFree(nextExpr[newItems])
                    newOperand.append(newOperA)
                    newOperand=NegationFree(newOperand)
                    retExpr.append(newOperand)
                return retExpr
                """ Transformation applying demorgans law ~(a V b) : (~a and ~b) and calls function recursively to find nested 'not' opertors"""

            else:
                print "Negation free invalid expression "
        else:
            return expr
    else:
        print "Negation free invalid expression "
        return

""" Function for distribution """
def Distribute(alpha,beta):
    print alpha
    print beta
    opAlpha = eval('alpha')
    opBeta = eval('beta')
    if(opAlpha[0]=='and'):
        retExpr = ['and']
        #tempExpr = []
        #retExpr = []
        for item in range(1,len(opAlpha)):
            x=Distribute(opAlpha[item],opBeta)
            temp=eval('x')
            if(temp[0]=='and'):
                for tempIndex in range(1,len(temp)):
                    retExpr.append(temp[tempIndex])
            else:
                retExpr.append(x)
        return retExpr
        """ case : when input 1st expression is having an 'and' operator
            input : (a and b) V c
            output :  (a V c) and (b V c) """
    if(opBeta[0]=='and'):
        retExpr = ['and']
        #retExpr = []
        for item in range(1,len(opBeta)):
            y=Distribute(opAlpha,opBeta[item])
            retExpr.append(y)
        return retExpr
        """ case : when input 2nd expression is having an 'and' operator
            input : a V (b and c)
            output :  (a V b) and (a V c) """
    else:
        #newListA = []
        #newListB = []
        retExp = ['or']
        if(opAlpha[0] == 'or'):
            for item in range(1,len(opAlpha)):
                retExp.append(opAlpha[item])
        else:
            retExp.append(opAlpha)
        if(opBeta[0] == 'or'):
            for item in range(1,len(opBeta)):
                retExp.append(opBeta[item])
        else:
            retExp.append(opBeta)
            
        #retExp.append(newListA)
        #retExp.append(newListB)
        return retExp
        """ case : applying associative rule
            input : (a V b) V c
            output : (a V b V c) """
        
""" Function for CNF conversion """
def convertCNF(stringExpr):
    print stringExpr
    if(len(stringExpr) > 1):
        expr = eval('stringExpr')
        optr = expr[0]
    else:
        optr=stringExpr

    if(len(optr) == 1):
        return optr
    elif(optr == 'and'):
        retExpr = ['and']
        for items in range(1,len(expr)):
            operand1 = convertCNF(expr[items])
            dupAnd = eval('operand1')
            if(dupAnd[0]=='and'):
                for index in range(1,len(dupAnd)):
                    retExpr.append(dupAnd[index])
            else:
                retExpr.append(dupAnd)
        return retExpr
    elif(optr == 'or'):
        newList=[]
        for items in range(1,len(expr)):
            operand1 = convertCNF(expr[items])
            newList.append(operand1)
        x=newList[0]
        for items in range(1,len(newList)):
            y=Distribute(x,newList[items])
            x=y
        retExpr = y
        return retExpr
        """ distribute or over ands """
        
    elif(optr == 'not'):
        return expr
    else:
        print "Distribution failed"
        return

""" handling duplicate item removal in subList """

def remDupSubList( cnfConvStmt ):
    cnfStmt = cnfConvStmt
    if(cnfStmt[0]=='and'):
        newOutterList=['and']
        for i in range(1,len(cnfStmt)):
            expri = eval('cnfStmt[i]')
            print expri
            if (expri[0] == 'or'):
                newInnerList=[]
                length=len(expri)
                j=1
                while (j < length):
                    a=expri[j]
                    print a
                    k=j+1
                    while(k<length):
                        b=expri[k]
                        print b
                        x=cmp(a,b)
                        if (x==0):
                            expri.pop(k)
                            length=length-1
                            k=k-1
                        k=k+1
                    newInnerList.append(a)
                    j=j+1
                if(len(newInnerList) == 1):
                    newOutterList.append(newInnerList[0])
                else:
                    newInnerList.insert(0,'or')
                    newOutterList.append(newInnerList)
            else:
                newOutterList.append(expri)
    else:
        newOutterList = cnfStmt
    return newOutterList


""" handling duplicate item removal in conjunctions and disjunctions """
"""newOutterList=['and', ['or', 'A', 'B'], ['or', 'D', 'A'], ['or', 'B', 'A'], ['or', 'A', 'D']]
   output = ['and', ['or', 'A', 'B'], ['or', 'D', 'A']]
"""

def conjDupRem( dupRemStmt ):
    newOutterList=dupRemStmt
    if(newOutterList[0]=='and' or newOutterList[0]=='or'):
        i=1;
        newFinalList=[]
        length = len(newOutterList)
        while(i < length):
            a=newOutterList[i]
            print a
            j=i+1
            while(j<length):
                b=newOutterList[j]
                print b
                count=0
                if(len(a) > 1):
                    if(len(b) != 1):                        
                        for k in range(0,len(a)):
                            if a[k] in b:
                                count=count+1
                else:
                    if a[0] in b:
                        count=count+1
                if((len(a)==len(b)) and (count==len(b))):
                    newOutterList.pop(j)
                    length=length-1
                    j=j-1
                j=j+1
            newFinalList.append(a)
            i=i+1
        if(len(newFinalList)>1):       
            if (newOutterList[0]=='and'):
                newFinalList.insert(0,'and')
            else:
                newFinalList.insert(0,'or')
        if(len(newFinalList) == 1):
            return newFinalList[0]
        else:
            return newFinalList
    else:
        if(len(dupRemStmt)==1):
            return dupRemStmt[0]
        else:
            return dupRemStmt

                   
""" Main program start """
#pdb.set_trace()
inputFile = open(sys.argv[2])
outputFile = open('sentences_CNF.txt', 'w')
firstLine = inputFile.readline()
N = int(firstLine)
for i in range(0,N):
    line = inputFile.readline()
    mystring = line.split('\n')[0]
    """ reading input expression from file """
    string = eval(mystring)
    """ removing 'iff' from input expression """
    biCondFreeStr = BiConditionalFree(string)
    print biCondFreeStr
    """ removing 'implies' from input expression """
    implFreeStr = ImpliesFree( biCondFreeStr )
    print implFreeStr
    """ removing 'not' from input expression """
    negFreeStr = NegationFree( implFreeStr )
    print negFreeStr
    """ applying distribution rule on input expression """
    cnfStmt = convertCNF(negFreeStr)
    print cnfStmt
    """ remove duplicate clause from cnf sentence """
    dupRemListStr = remDupSubList( cnfStmt )
    print dupRemListStr
    finalListStr = conjDupRem(dupRemListStr)
    print finalListStr
    finalStr = (str(finalListStr)) + "\n"
    """write the output to the file """
    outputFile.write(finalStr)

inputFile.close()
outputFile.close()
