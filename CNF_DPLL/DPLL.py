import pdb
from heapq import *
import sys
from collections import defaultdict

""" Function for extracting symbols from the input cnf sentence
    input : ['and', 'A', 'B', 'C']
    output of this function : {'A' : '', 'B' : '', 'C' : ''}"""

def extractSymbols(stringExpr):
    
    print stringExpr
    global symbol
    symbol = {}
    if(len(stringExpr)==1):
        z=eval('stringExpr')
        symbol[z[0]] = ''
        return symbol
    
    for i in range(1,len(stringExpr)):
        symbolStr = eval('stringExpr[i]')
        if (symbolStr[0] == 'or') :
            for j in range(1,len(symbolStr)):
                z=eval('symbolStr[j]')
                if(len(symbolStr[j])==2):
                    if z[1] not in symbol:
                        symbol[z[1]] = ''
                else:
                    if z[0] not in symbol:
                        symbol[z[0]] = ''
        if (symbolStr[0] == 'not'):
            z=eval('symbolStr')
            if(z[1] not in symbol):
                symbol[z[1]] = ''
        if (len(symbolStr)==1):
            z=eval('symbolStr')
            if (z[0] not in symbol):
                symbol[z[0]] = ''
    return symbol
            

"""function for finding the unit clause in the input CNF expression """
""" input : ['and','A',['not', 'B'], ['or','B','C']]
    output : returns 'A' to the calling function """

def findUnitClause(StringExpr):
    print StringExpr
    clauses = eval('StringExpr')
    if (len(StringExpr)==1 and (not StringExpr[0])):
        return []
    if not StringExpr:
        return[]
    if (clauses[0] == 'not' or len(clauses)==1):
        return clauses
    for i in range(0,len(clauses)):
        if (clauses[i] != 'and' and clauses[i] and clauses[i] != 'or'):
            clauseOp = eval('clauses[i]')
            if (clauseOp[0]=='not'):
                return clauseOp
            else:
                if(len(clauseOp) == 1):
                    return clauseOp
    return []

"""This function remove all clauses that contain the unit symbol fetched
    by the previous function """

def removeClauses(String,unitClause):
    StringExpr = eval('String')
    print StringExpr
    print unitClause
    retExpr = []
    if(StringExpr[0]=='or'):
        #tempexp = []
        #tempexp.append(StringExpr)
        if(unitClause in StringExpr):
            return retExpr
        else:
            return String
    if (cmp(unitClause,String)==0):
        return retExpr
    for i in range(0,len(StringExpr)):
        if (StringExpr[i] != 'and'):
            if(len(unitClause) == 1):
                if (unitClause[0] in StringExpr[i]):
                    if (len(StringExpr[i])==2):
                        retExpr.append(StringExpr[i])
                else:
                    retExpr.append(StringExpr[i])
            else:
                if (len(StringExpr[i])!=1):
                    if (unitClause not in StringExpr[i]):
                        if (cmp(unitClause,StringExpr[i])!=0):
                            retExpr.append(StringExpr[i])
                else:
                    retExpr.append(StringExpr[i])
    return retExpr
   
""" This function removes the compliment literals of the unit clause from the clauses
    input : ['and', ['or','A',B'], ['not','A']]
    unit clause : ['not','A'], compliment : 'A' 
    output : 'B'   """


def removeCompClauses(String,compClause):
    StringExpr = eval('String')
    print StringExpr
    print compClause
    #if(StringExpr[0]=='not' or
    retCompFree = []
    if not StringExpr :
        return retCompFree
    if (compClause[0]=='not' and StringExpr[0]=='not'):
        if(cmp(StringExpr,compClause)==0):
            x=formatExpr(StringExpr,compClause)
            retCompFree.append(x)
            return retCompFree
        else:
            return StringExpr
                   
  
    for i in range(0,len(StringExpr)):
        if(len(compClause) == 1):
            if(compClause[0] in StringExpr[i]):
                if(len(StringExpr[i]) !=2):
                    x=formatExpr(StringExpr[i],compClause)
                    print x
                    retCompFree.append(x)
            else:
                retCompFree.append(StringExpr[i])
        else:
            if (len(StringExpr[i])!=1):
                if(StringExpr[i]!='or'):
                    if (compClause in StringExpr[i]):
                        x=formatExpr(StringExpr[i],compClause)
                        print x
                        retCompFree.append(x)
                    else:
                        if(cmp(compClause,StringExpr[i])==0):
                            x=formatExpr(StringExpr[i],compClause)
                            print x
                            retCompFree.append(x)
                        else:
                            retCompFree.append(StringExpr[i])
            else:
                
                retCompFree.append(StringExpr[i])
                

    if(len(retCompFree)==1 and retCompFree[0]):
        return retCompFree[0]
    else:
        return retCompFree            

""" formats the string after removing the compliment literal from the clause"""
def formatExpr(StringExpr,compClause):
    print StringExpr
    print compClause
    singleClause = eval('StringExpr')
    retClause = []
    if (singleClause[0]=='not'or len(singleClause)==1):
        return retClause
    else:
        for j in range(1,len(singleClause)):
            singleValue = eval('singleClause[j]')
            if(len(compClause) == 1):
                if (compClause  not in singleValue):
                    retClause.append(singleValue)
                else:
                    if(len(compClause) != len(singleValue)):
                        retClause.append(singleValue)
            else:
                if(cmp(compClause,singleValue)!=0):
                    retClause.append(singleValue)
        if (len(retClause) == 1):            
            return retClause[0]
        else:
            retClause.insert(0,'or')
            return retClause

""" DPLL ALGORITHM """

def dpll(StringExpr):
    global symbol
    print StringExpr
    unitPresent = True
    while unitPresent:
        unitClause = findUnitClause(StringExpr)
        print unitClause
        if unitClause:
            x=eval('unitClause')
            if(x[0]=='not'):
                symbol[x[1]]=False
            else:
                symbol[x[0]]=True

            unitFreeExpr = removeClauses(StringExpr,unitClause)
            print unitFreeExpr
            if (x[0] == 'not'):
                compClause=x[1]
            else:
                compClause=["not",x[0]]
            compFreeExpr = removeCompClauses(unitFreeExpr,compClause)
            print compFreeExpr
            StringExpr = compFreeExpr
        else:
            unitPresent=False

    if not StringExpr:
        return True
    
    for i in range(0,len(StringExpr)):
        if not StringExpr[i]:
            return False
        
    if(StringExpr[0]!='and'):   
        nextOpr = eval('StringExpr[0]')
    else:
        nextOpr = eval('StringExpr[1]')
    newOp = nextOpr[1]
    
    if (len(newOp) == 2):
        """Splitting rule setting literal = True """
        StringExpr.append(newOp)
        dplX = dpll(StringExpr)
        if (dplX == True):
            return True
        else:
            StringExpr.append(newOp[1])
            StringExpr.pop()
            """Splitting rule setting literal = False """
            dplX=dpll(StringExpr)
            if (dplX == True):
                return True
            else:
                return False
    else:
        StringExpr.append(newOp)
        """Splitting rule setting literal = True """
        dplY = dpll(StringExpr)
        if dplY == True:
            return True
        else:
            y=['not',newOp]
            StringExpr.pop()
            StringExpr.append(y)
            """Splitting rule setting literal = False """
            """ back tracking recursive calls """
            dplY = dpll(StringExpr)
            if dplY == True:
                return True
            else:
                return False
    
""" function to format the satisfiability assignment """

def formatResult():
    global symbol
    result = []
    symbolItems = symbol.items()
    for singleSymItem in symbolItems:
        if (singleSymItem[1] != ''):
            if(singleSymItem[1] == True):
                strRes = singleSymItem[0]+"="+"True"
            else:
                strRes = singleSymItem[0]+"="+"False"
        else:
            strRes = singleSymItem[0]+"="+"True"
        result.append(strRes)
    return result   

        
""" DPLL Program start """

#pdb.set_trace()
inputFile = open(sys.argv[2])
outputFile = open('CNF_satisfiability.txt', 'w')
firstLine = inputFile.readline()
N = int(firstLine)
for i in range(0,N):
    line = inputFile.readline()
    mystring = line.split('\n')[0]
    string = eval(mystring)
    global symbol
    symbol = {}
    symbol = extractSymbols(string)
    print symbol
    dpllSat = dpll(string)
    print dpllSat
    retResult=[]
    if (dpllSat == True):
        retResult=formatResult()
    retResult.insert(0,dpllSat)
    print retResult
    finResult = (str(retResult)) + '\n'
    outputFile.write(finResult)

inputFile.close()
outputFile.close()

